const getBoardInfoById = require("./callback1.js");
const getListsOfBoard = require("./callback2.js");
const getCardsOfList = require("./callback3.js");

const getInfoListAndCards = (boardName, boards, lists, cards) => {
  setTimeout(() => {
    boards.filter((board) => {
      if (board.name == boardName) {
        getBoardInfoById(board.id, boards, (boardData) => {
          if (boardData) {
            console.log(`Board Information for ${boardName}:`);
            console.log(boardData);

            getListsOfBoard(board.id, lists, (listData) => {
              if (listData) {
                console.log(`\nLists for ${boardName} board:`);
                console.log(listData);

                listData.forEach((list) => {
                  getCardsOfList(list.id, cards, (cardsData) => {
                    if (cardsData) {
                      console.log(`\nCards for ${list.name} list:`);
                      console.log(cardsData);
                    } else {
                      console.log(`No cards found for ${list.name}`);
                    }
                  });
                });
              } else {
                console.log("Invalid ID");
              }
            });
          } else {
            console.log("Invalid Board Name");
          }
        });
      }
    });
  }, 2000);
};

module.exports = getInfoListAndCards;
