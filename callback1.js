const getBoardInfoById = (boardId, boards, callback) => {
  setTimeout(() => {
    callback(boards.find((board) => board.id == boardId));
  }, 2000);
};

module.exports = getBoardInfoById;
