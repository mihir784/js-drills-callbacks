const getCardsOfList = (listId, cards, callback) => {
  setTimeout(() => {
    callback(cards[listId]);
  }, 2000);
};

module.exports = getCardsOfList;
