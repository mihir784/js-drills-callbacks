const lists = require("../data/lists.json");
const getListsOfBoard = require("../callback2.js");

getListsOfBoard("abc122dc", lists, (boardList) => {
  if (boardList) {
    console.log(boardList);
  } else {
    console.log("Invalid ID");
  }
});
