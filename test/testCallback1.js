const boards = require("../data/boards.json");
const getBoardInfoById = require("../callback1.js");

getBoardInfoById("abc122dc", boards, (board) => {
  if (board) {
    console.log(board);
  } else {
    console.log("Invalid ID");
  }
});
