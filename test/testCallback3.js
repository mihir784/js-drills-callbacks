const cards = require("../data/cards.json");
const getCardsOfList = require("../callback3.js");

getCardsOfList("ghnb768", cards, (cardList) => {
  if (cardList) {
    console.log(cardList);
  } else {
    console.log("Invalid ID");
  }
});
