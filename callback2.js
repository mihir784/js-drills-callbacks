const getListsOfBoard = (boardId, lists, callback) => {
  setTimeout(() => {
    callback(lists[boardId]);
  }, 2000);
};

module.exports = getListsOfBoard;
